#ifndef traversal
#define traversal

#include "randomtree.h"
void preorder(struct node *root){
    if(root == NULL) 
        return;
    else{
        cout << root->value << " ";
        preorder(root->left);
        preorder(root->right);
    }
}

void inorder(struct node *root){
    if(root == NULL)
        return;
    else{
        inorder(root->left);
        cout << root->value << " ";
        inorder(root->right);
    }
}

void postorder(struct node *root){
    if(root == NULL)
        return;
    else{
        postorder(root->left);
        postorder(root->right);
        cout << root->value << " ";
    }
}
#endif