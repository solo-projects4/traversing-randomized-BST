#include "randomtree.h"
#include "traversal.h"
using namespace std;

// checking if input is a number
bool isNumber(const string& input){
    for(char const &ch : input){
        if(isdigit(ch) == 0){
            return false;
        }
    }
    return true;
}

int main(void){
    srand(time(NULL));// "seed" for random numbers
    string choice;
    node *root = NULL;
    while(1){
        cout << endl;// formatting purposes
        cout << "1.Generate Random Tree" << endl;
        cout << "2.Print preorder traversal (NLR)" << endl;
        cout << "3.Print inorder traversal (LNR)" << endl;
        cout << "4.Print postorder traversal (LRN)" << endl;
        cout << "5.Quit"<<endl;
        cout << "Enter a number between 1 and 5: " << endl;
        do{
            cin >> choice;
            if(isNumber(choice) == false){
                cout << "Enter a NUMBER" << endl;
            }  
        }while(isNumber(choice) == false);

        switch(stoi(choice))// casting string to int 
        {
            case 1:
                {
                    root = generateRandomTree();
                    break;
                }
            case 2:
                {
                    if(root == NULL){
                        cout << "Tree is not generated." << endl;
                        continue;
                    }
                    cout << "preorder: ";
                    preorder(root);
                    cout << endl;
                    break;
                }
            case 3:
                {
                    if(root == NULL){
                        cout << "Tree is not generated." << endl;
                        continue;
                    }
                    cout << "inorder: ";
                    inorder(root);
                    cout << endl;
                    break;
                }
            case 4:
                {
                    if(root == NULL){
                        cout << "Tree is not generated." << endl;
                        continue;
                    }
                    cout << "postorder: ";
                    postorder(root);
                    cout << endl;
                    break;
                }
            case 5:
                {
                    exit(1);
                }
            default:
                {
                cout << "Not an option. Pick a number between 1 and 5." << endl;
                }
        }
    }
    return 0;
}
