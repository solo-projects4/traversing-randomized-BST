#ifndef randomtree
#define randomtree

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include "randomtree.h"
using namespace std;

struct node
    {
        int value;
        struct node *left;
        struct node *right;
    };

void insertNodeInTree(struct node *root, struct node *nodeToBeInserted){
    if((root->right == NULL) && (nodeToBeInserted->value > root->value)){
        root->right = nodeToBeInserted;
        std::cout << nodeToBeInserted->value << " is right child of " << root->value << std::endl;
    }
    else if((root->right != NULL) && nodeToBeInserted->value > root->value){
        insertNodeInTree(root->right, nodeToBeInserted);
    }
    if((root->left == NULL) && (nodeToBeInserted->value < root->value)){
        root->left = nodeToBeInserted;
        cout << nodeToBeInserted->value << " is left child of " << root->value << endl;
    }
    else if((root->left != NULL) && (nodeToBeInserted->value < root->value)){
        insertNodeInTree(root->left, nodeToBeInserted);
    }
}

node* generateRandomTree(){

    cout << "generating random binary search tree";
    // artificial time delay for terminal gui
    for (int i = 0; i < 4; i++){
        Sleep(1000);// parameter in miliseconds
        cout << ".";
    }
    cout << endl;

    node* data = (node*)calloc(10, sizeof(node));
    // number of elements is between 5 and 10
    int randomNumberOfElements = 5 + (rand() % (10 - 5 + 1));
    cout << "No. of nodes: " << randomNumberOfElements << endl;
    for(int i = 0; i < randomNumberOfElements; i++){
        // value of each item is between 1 and 50 
        (data + i)->value = 1 + (rand() % 50);
    }

    // duplicate values handling
    for(int i = 0; i < randomNumberOfElements; i++){
        for(int j = i+1; j < randomNumberOfElements; j++){
            if ((data + j)->value == (data + i)->value){
                // generating new values in case of a duplicate node value
                do{
                    (data+j)->value = 1 + (rand() % 50);
                }while((data + j)->value == (data + i)->value);
            }
        }
    }
    
    node* root = (node*)calloc(1, sizeof(node));
    root->value = (data + 0)->value;
    root->left = NULL;
    root->right = NULL;
    cout << "root value: " << root->value << endl;
    cout << "other node values: ";
    for (int i = 1; i < randomNumberOfElements; i++){
        cout << (data + i)->value << " ";
    }
    cout << endl;
    for (int i = 1; i < randomNumberOfElements; i++){
        insertNodeInTree(root, data + i);
    }
    
    return root;
    
    free(data);
    free(root);
}

#endif