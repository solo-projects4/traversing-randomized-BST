# Traversing Randomized BST

BST stands for binary search tree. Tree is a type of a data structure. It is hierarchal data arranged in a tree-like structure.
We distinguish two types of items in a tree - nodes and leaves, connected with so-called branches.
Node(parent) is more complex as it stores different nodes and/or leaves. Leaves(children) are items that usually do some type of a task.

Binary Search Tree is a binary tree(tree data structure in which each node has at most two children) which has the following properties:

    1. The left subtree of a node contains only nodes with keys lesser than the node’s key.
    2. The right subtree of a node contains only nodes with keys greater than the node’s key.
    3. The left and right subtree each must also be a binary search tree.

In this project, tree structure is randomized (as you can see in generateRandomTree() method inside randomtree.h - custom made header file).
Randomization works like this: 

    - number of tree elements is between 5 and 10
    - value of each node is between 1 and 50

Also, you can see different types of tree traversing. Tree traversal is a way of visiting every node in a tree data structure only once.
There are three main strategies for traversing a tree - inorder(left, root, right), preorder(root, left, right) and postorder(left, right, root).

Algorithms:

    Inorder(tree)
        1. Traverse the left subtree, i.e., call Inorder(left-subtree)
        2. Visit the root.
        3. Traverse the right subtree, i.e., call Inorder(right-subtree)

    Preorder(tree)
        1. Visit the root
        2. Traverse the left subtree, i.e., call Preorder(left-subtree)
        3. Traverse the right subtree, i.e., call Preorder(right-subtree) 
    
    Postorder(tree)
        1. Traverse the left subtree, i.e., call Postorder(left-subtree)
        2. Traverse the right subtree, i.e., call Postorder(right-subtree)
        3. Visit the root.

You can see the examples of a program in attached images. Enjoy!
